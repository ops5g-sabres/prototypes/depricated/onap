#!/usr/bin/python3

import os # file manipulation
import fileinput # replacing lines of text
import shutil # creating backup file

def apply():
    for root, _, files in os.walk("/home/test/onap/oom/kubernetes"):
        for fi in files:
            if fi.endswith("yaml") or fi.endswith("yml"):
                replace(fi)
            if fi.endswith("deployment.yaml") or fi.endswith("deployment.yml"):
                fixKube(fi)

def test():
    replace("test.yml", modify=False)
    fixKube("test.yml", modify=False)

# this could be done with sed and added to git patch, but this may be a better
# solution for if we bump and their code is still using out of date k8 api.
def replace(filename, modify):
    if modify:
        with fileinput.FileInput(filename, inplace=True, backup='.bak') as fi:
            for line in fi:
                if "v1beta1" in line:
                    if "extensions/v1beta1" in line:
                        line.replace("extensions/v1beta1", "apps/v1")
                    elif "batch/v1beta1" in line:
                        line.replace("batch/v1beta1", "batch/v1")
                    elif "networking.k8s.io/v1beta1" in line:
                        line.replace("networking.k8s.io/v1beta1", "networking.k8s.io/v1")
                    elif "rbac.authorization.k8s.io/v1beta1" in line:
                        line.replace("rbac.authorization.k8s.io/v1beta1", "rbac.authorization.k8s.io/v1")
    else:
        with open(filename, 'r') as fi:
            num = 0
            for line in fi:
                if "v1beta1" in line:
                    if "extensions/v1beta1" in line:
                        line.replace("extensions/v1beta1", "apps/v1")
                    elif "batch/v1beta1" in line:
                        line.replace("batch/v1beta1", "batch/v1")
                    elif "networking.k8s.io/v1beta1" in line:
                        line.replace("networking.k8s.io/v1beta1", "networking.k8s.io/v1")
                    elif "rbac.authorization.k8s.io/v1beta1" in line:
                        line.replace("rbac.authorization.k8s.io/v1beta1", "rbac.authorization.k8s.io/v1")
                print(num, line, end='')
                num += 1

# the kubernetes file is not formatted correctly, we need to update the spec
####
#spec:
#  template:
#    metadata:
#      labels:
#        app: {{ include "common.name" . }}
#        release: {{ include "common.release" . }}
####
####
#spec:
#  selector:
#    matchLabels:
#      app: {{ include "common.name" . }}
####
def fixKube(filename, modify):
    print("fixing kube")

    # 1. whoever wrote these templates....
    # not parsable with jinja/yaml, so text replace it is
    with open(filename, 'r') as yfi:
        app = ""
        release = ""

        tracker = {'a': [], 'r': []}
        linenum = 0
        for line in yfi:
            # find what are indendation is at
            counter = len(line) - len(line.lstrip())

            # create keys in dict to track when we found
            # the key words.
            if 'template:' in line:
                tracker['t'] = counter
                tracker['tl'] = linenum
                if 's' not in tracker:
                    tracker['f'] = 't'

            if 'selector:' in line:
                tracker['s'] = counter
                tracker['sl'] = linenum
                if 't' not in tracker:
                    tracker['f'] = 's'

            if 'spec:' in line and 'specl' not in tracker:
                tracker['specl'] = linenum
				tracker['spec'] = counter

            # make bounds for the yaml sections
            if 'te' not in tracker and 't' in tracker:
                if counter <= tracker['t'] and tracker['tl'] != linenum:
                    tracker['te'] = linenum

            if 'se' not in tracker and 's' in tracker:
                if counter <= tracker['s'] and tracker['sl'] != linenum:
                    tracker['se'] = linenum

            # finding the keyword and checking bounds
            if 'app:' in line:
                tracker['a'] += [linenum]
                if 't' in tracker and 'te' not in tracker:
                    app = line

            if 'release:' in line:
                tracker['r'] += [linenum]
                if 't' in tracker and 'te' not in tracker:
                    release = line

            if 'matchLabels' in line:
                tracker['ml'] = linenum

            linenum += 1

    # configure what the indent should be based off of known values.
    inc = 2
    if 's' in tracker:
        inc = tracker['spec']  - tracker['s']
    elif 't' in tracker:
        inc = tracker['spec']  - tracker['t']

    # prepare the yaml to insert
    incl = ""
    if 'ml' not in tracker:
        if 's' not in tracker:
            incl = ''*inc+"selector:\n"
            incl += ''*(inc*2)+"matchLabels:\n"
            incl += ''*(inc*3)+app
            incl += ''*(inc*3)+release
        else:
            incl += ''*(inc+tracker['s'])+"matchLabels:\n"
            incl += ''*(inc*2+tracker['s'])+app
            incl += ''*(inc*2+tracker['s'])+release

	shutil.copyfile(filename,filename+'.bak')

	# prepare where to put it.
	dels = {}
    if 'ml' not in tracker:
        if 's' not in tracker:
			ins = tracker['specl']
        else:
			ins = tracker['sl']
    else:
		for a in tracker['a']:
			if a > tracker['ml'] and a < tracker['sl']:
				dels['a'] = a
		for r in tracker['r']:
			if r > tracker['ml'] and r < tracker['sl']:
				dels['r'] = r

	dels['ar'] = ''*(inc)+app
    dels['rr'] = ''*(inc)+release

	ficontents = ""
    with open(filename+".bak", 'r') as rfi:
		ficonents = rfi.readlines()

	#TODO to handle this replacement

    with open(filename, 'w') as rfi:
		linenum = 0
		newline = ""
		for line in ficontents:
			newline = line
			if 'a' dels:
				if dels['a'] == linenum:
					newline = dels['ar']
			else:
				rfi.write(line)



def main():
    test()

if __name__ == "__main__":
    main()


