#!/bin/bash

if [[ "$#" -ne 3 ]]; then
	printf "`basename $0` \$IP \$USER \$PW\n"
	printf "`basename $0` 192.168.1.148 test test\n"
	exit 0
fi

if !sshpass; then
	printf "sshpass needs to be installed"
fi

ip=$1
user=$2
pw=$3

printf "ip: $ip\n"
printf "user: $user\n"
printf "pw: $pw\n"

rm -f ~/.ssh/ansible-temp*

ssh-keygen -f ~/.ssh/ansible-temp -t rsa -b 4096 -N ""

sleep 1

sshpass -p $pw ssh-copy-id \
  -o StrictHostKeyChecking=no \
  -i ~/.ssh/ansible-temp $user@$ip 

sed -i "s/ansible_host:.*$/ansible_host: $ip/g" inventory.yml

ssh -t -i ~/.ssh/ansible-temp $user@$ip \
	    -o StrictHostKeyChecking=no \
	    "echo \"$pw\" | sudo -S sed -i 's/%sudo.*$/%sudo\tALL=(ALL)\tNOPASSWD:ALL/g' /etc/sudoers"

