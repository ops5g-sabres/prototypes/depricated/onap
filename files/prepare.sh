#!/bin/bash

# from: https://developer.ibm.com/technologies/containers/tutorials/deploy-onap-beijing-on-ibm-cloud-private/ 

mkdir ~/onap
cd ~/onap

# Get the cd.sh script
wget https://git.onap.org/logging-analytics/plain/deploy/cd.sh
chmod +x cd.sh

# Set up the local/onap helm charts used in cd.sh for ONAP master branch
# git clone http://gerrit.onap.org/r/oom
git clone -b frankfurt http://gerrit.onap.org/r/oom --recurse-submodules
cd oom/kubernetes
git submodule update --init --recursive
helm serve &
make all

# Get ONAP parameters values file
cd ~/onap
wget https://jira.onap.org/secure/attachment/11414/values.yaml
